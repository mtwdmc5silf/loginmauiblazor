﻿using LoginMAUIBlazor.Models;

namespace LoginMAUIBlazor.Interfaces
{
    internal interface IItem
    {
        Task<IEnumerable<Item>> getAll(string JWT);
    }
}
