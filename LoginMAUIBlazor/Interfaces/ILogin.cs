﻿using LoginMAUIBlazor.Models;

namespace LoginMAUIBlazor.Interfaces
{
    interface ILogin
    {
        Task<Login> Authenticate(UserMin userMin);
    }
}
