﻿using LoginMAUIBlazor.Interfaces;
using LoginMAUIBlazor.Models;
using Newtonsoft.Json;

namespace LoginMAUIBlazor.Services
{
    public class ItemService : IItem
    {
        System.Net.Http.HttpClient client;
        string WebAPIUrl = string.Empty;
        public ItemService()
        {
            client = new System.Net.Http.HttpClient();
        }
        public async Task<IEnumerable<Item>> getAll(string JWT)
        {
            WebAPIUrl = "https://localhost:7283/api/Item";
            var uri = new Uri(WebAPIUrl);
            try
            {                
                client.DefaultRequestHeaders.Add("Authorization", JWT);
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var resultado = JsonConvert.DeserializeObject<IEnumerable<Item>>(content);
                    return resultado;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return null;
        }
    }
}
