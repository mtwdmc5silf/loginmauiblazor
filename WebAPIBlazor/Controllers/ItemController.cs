﻿using Microsoft.AspNetCore.Mvc;
using WebAPIBlazor.Models;
using WebAPIBlazor.Services;

namespace WebAPIBlazor.Controllers
{
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IAuthService _authService;
        public ItemController(IAuthService authService)
        {
            _authService = authService;
        }
        [HttpGet]
        [Route("api/[controller]")]
        //public async Task<ActionResult<IEnumerable<DestinoPGH>>> GetAll()
        public async Task<ActionResult> GetAll()
        {
            string token = HttpContext.Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrEmpty(token))
                return Unauthorized();
            var userNick = _authService.validateToken(token);
            if (string.IsNullOrEmpty(userNick))
                return Unauthorized();
            var listItem = new List<Item>() {
                new Item { ID =1, descripcion = "abc" }
                , new Item { ID =2, descripcion = "qwe" }
            };
            return Ok(listItem);
        }
    }    
}
