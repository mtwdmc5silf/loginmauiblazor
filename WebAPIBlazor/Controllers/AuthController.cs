﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPIBlazor.Models;
using WebAPIBlazor.Services;

namespace WebAPIBlazor.Controllers
{
	[ApiController]

	public class AuthController : ControllerBase
	{
		private readonly IAuthService _authService;
		public AuthController(IAuthService authService)
		{
			_authService = authService;
		}

		[HttpPost]
		[Route("api/[controller]")]
		public async Task<ActionResult<Login>> Auth([FromBody] UserMin user) {
			if (user == null)
				return BadRequest();			
			Login login = new Login();
			login = await _authService.Auth(user);
			return login;
		}		
	}
}
