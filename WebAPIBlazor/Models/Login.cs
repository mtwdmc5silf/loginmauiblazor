﻿namespace WebAPIBlazor.Models
{
    public class Login : User
    {
        public string Token { get; set; }
    }
}
