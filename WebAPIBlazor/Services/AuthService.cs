﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebAPIBlazor.Models;

namespace WebAPIBlazor.Services
{
	public class AuthService : IAuthService
    {
        const string SECRET_KEY = "Hola mundo este es la palabra secret para el JWT";
        public async Task<Login> Auth(UserMin user)
        {
            Login login = new Login()
            {
                ID = 1,
                Name = "sergio lopez",
                Nick = user.Nick,
                Password = ""
            };
            if (login.ID > 0)
            {
                login.Token = GenerateJwtToken(user);
            }
            return login;
        }
        private string GenerateJwtToken(UserMin user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SECRET_KEY);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {                    
                    new Claim("userNick", user.Nick?.ToString() ?? "")
                }),
                Expires = System.DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };            
            //if (session.Roles != null)
            //    foreach (string rol in session.Roles)
            //        tokenDescriptor.Subject.AddClaim(new Claim("Roles", rol));
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        public string validateToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = System.Text.Encoding.ASCII.GetBytes(SECRET_KEY);
                tokenHandler.ValidateToken(token, new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero,
                }, out SecurityToken validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                var userNick = jwtToken.Claims.First(x => x.Type == "userNick").Value;
                return userNick;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
