﻿using WebAPIBlazor.Models;

namespace WebAPIBlazor.Services
{
	public interface IAuthService
	{
		Task<Login> Auth(UserMin auth);
		public string validateToken(string token);

	}
}
